# python has several structures to store collections or multiple items in a single variable
# List[]. dictionary, tuples(), set{}
# Tuples - ("Math", "Sicence","English")
# Set - {"Math", "Science", "English"}

# [Section] Lists
# Lists are similar to arrays of JS in a sense that they can contain a collection of data
# To create a list , the square bracket ([]) is used.


names = ["John","George","Ringo"] #String list

programs = ["developer career","pi-shape","short courses"] #string list



truth_variables = [True, False, True, True, False] #bolean list

#A list can contain elements with different data types:

sample_list = ["Apple", 3, False, "Potato", 4, False]

# note: problem may arise if you try to use lists with multiple type for something that expects them to be all the same type

print(len(programs))

var_len = len(sample_list)

print(var_len)


print(names[0])
print(names[-1])
print(programs)


print(programs[0:2])

# Updating lists
print (f"Current value: {programs[2]}")

# update the value
programs[2] = "ShortCourses"
print(f"New value: {programs[2]}")

# List has methods that can be used to manipulate the elements within
# Adding List Items - the append() method allows to insert items to a list

programs.append("global")

print(programs)

# deleting List items - the "del" keyword can be used to delete elements in the list

durations = [260 , 180, 20] #number list

durations.append(360)

print(durations)

del durations[-1]
print(durations)

del durations[0]
print(durations)

#insert method

durations.insert(0, 100)
print(durations)

durations.insert(1, 99)
print(durations)

# Membership checking - the "in" keyword checks 

print(20 in durations) # true value
print(500 in durations) # false value

# Sorting lists - the sort() method sorts the list alphaneumerically, ascending, by default

durations.sort()
print(durations)


#emptying the list - clear() method is used to emptying the content of the list

test_list = [1,3,5,7,9]
print(test_list)

test_list.clear()
print(test_list)




person1 = {
	"name" : "Brandon",
	"age" : 28,
	"occupation" : "students",
	"isEnrolled" : True,
	"subjects" : ["Phython", "SQL", "Django"]
}

print(len(person1))
print(person1["name"])

print(person1.keys())

print(person1.values())

#The items() method will return each item in a dictionary as key-value pair in a list

print(person1)
print(person1.items())

#Adding key-value pairs can be done with either a new index key and assigning a value or the update method

#index key

person1["nationality"]="Filipino"
print(person1)

person1.update({"fave_food":"Sinigang"})
print(person1)

#Deleting entries
# using pop method
person1.pop("name")
print(person1)

#using del keyword
del person1["nationality"]
print (person1)

# the clear() method empties the dictionary 
person2 = {
	"name" : "John",
	"age" : 18
}

person2.clear()
print(person2)

#looping through

for key in person1 :
	print(f"The value of {key} is {person1[key]}")

	person3 = {
	"name":"Monika",
	"age":20,
	"occupation": "poet",
	"isEnrolled":True,
	"subject":["python","SQL","django"]
	}

print(person3["subject"][0])
print(person3["subject"][-1])

class_room = {
	"student1" : person1,
	"student2" : person3
}

print(class_room["student2"]["subject"][1])


# [Section] Functions
# Functions are block of code that runs when called.
# A fuction can be used to get inputs, process inputs and return inputs.
# the "def" key is used to create a function. The syntax:
# def <function_name>()

def my_greeting():
	print("Hello User!")

my_greeting()

def greet_user(username):
	print(f"Hello,{username}")


greet_user("Darwin")

def addition(num1, num2):
	return num1 + num2

sum = addition(5,9)
print(f"the sum is {sum}!")

# [Section] Lambda Functions
# A lamda function is a small anonymous function cna be used for callbacks
# It is just any normal python function, except that it has no name when defining it, and it is contained in one of code

greeting = lambda person: f"hello {person}"

greeting ("Elsie")

print(greeting("Elsie"))


mult = lambda a, b : a * b


print(mult(2,3))

# [Section] Classes
# Classes would serve as blueprints to describe the concept of objects
# Each Object has characteristics (properties) and behaviors (method)

class Car():
	# properties that 

	# Initializing the properties of our function
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make
		self.fuel = "Gasoline"
		self.fuel_level = 0

	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print(f"filling up the tank . . .")
		self.fuel_level = 100
		print(f"new fuel level: {self.fuel_level}")

	def drive(self, distance):
		print(f"The car has driven {distance} kilometers!")
		self.fuel_level = self.fuel_level - distance
		print(f"The fuel left: {self.fuel_level}")

new_car = Car("Nissan", "GT-R", 2019)

print(new_car.brand)
print(new_car.model)
print(new_car.year_of_make)
print(new_car)

new_car.fill_fuel()
print(new_car.fuel_level)

new_car.drive(50)